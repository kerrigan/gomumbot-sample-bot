package main

import (
	"flag"
	"fmt"
	"os"
	"os/signal"
	"runtime"
	"syscall"

	vhandler "bitbucket.org/kerrigan/gomumbot-voice/recbothandler"
	mumbot "bitbucket.org/kerrigan/gomumbot/mumbot"
)

func main() {
	runtime.GOMAXPROCS(50)

	/*
		f, err := os.Create("profile.prof")
		if err != nil {
			log.Fatal(err)
		}
	*/

	/*
		pprof.StartCPUProfile(f)
		defer pprof.StopCPUProfile()
	*/

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	signal.Notify(c, syscall.SIGTERM)

	var botNick = flag.String("nickname", "gomumbot", "ник бота")
	var ip = flag.String("ip", "localhost", "ip-адрес сервера mumble")
	var port = flag.Int("port", 64738, "порт сервера mumble")
	var icecastHost = flag.String("icecasthost", "localhost", "адрес icecast")
	var iceUser = flag.String("iceuser", "mountuser", "пользователь icecast")
	var icePassword = flag.String("icecpass", "mountpassword", "пароль icecast")
	var iceMount = flag.String("mount", "/mpd.ogg", "адрес маунта")
	var icePort = flag.Int("iceport", 8000, "порт icecast")
	flag.Parse()

	bot := mumbot.NewMumbleBot(*ip, *port, *botNick)
	mh := mumbot.SimpleMessageHandler{}

	vh1 := vhandler.NewRecBotHandler(*icecastHost, *iceUser, *icePassword, *iceMount, *icePort)

	bot.SetMessageHandler(&mh)
	bot.SetVoiceHandler(vh1)

	//go vh.Run()

	vh1.Run()
	//pprof.WriteHeapProfile(f)

	go func() {

		for _ = range c {
			bot.Stop()
			vh1.Stop()
			fmt.Println("Stopping mumble bot.")
			os.Exit(0)
		}
	}()

	bot.Run()
}

/*
func NewBot(ip, nick string, port int) {
	bot1 = mumbot.NewMumbleBot(ip, port, nick)

}

func Run() {
	bot1.Run()
}

func Stop() {
	bot1.Stop()
}
*/
